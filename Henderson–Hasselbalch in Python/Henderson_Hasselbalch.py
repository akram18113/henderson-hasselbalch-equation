import math

def henderson_hasselbalch(pka, acid, base):
    ratio = base/acid
    ph = pka + math.log10(ratio)
    return ph


acid = 0.1
base = 0.2
pka = 4.8

ph = henderson_hasselbalch(pka, acid, base)
print("pH of the buffer solution: ", ph)
